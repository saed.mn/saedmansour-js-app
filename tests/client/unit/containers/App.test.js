import React from 'react';
import App from '../../../../client/containers/App.js';
import Menu from '../../../../client/components/Menu.js';
import {Router} from 'react-router-dom';
import { mount } from 'enzyme';
import { expect } from 'chai';
import { Provider } from 'react-redux';

describe('<App />', () => {

	let app;

	beforeEach(() => {
		const storeFake = (state) => {
			return {
				default: () => {},
				subscribe: () => {},
				dispatch: () => {},
				getState: () => {
					return {
						auth: {
							isAuthenticated: false
						}
					};
				},
			};
		};

		app = mount(<Provider store={storeFake()}><App /></Provider>);
	});

  it('Renders an #app', () => {
    expect(app.find('#app')).to.have.length(1);
  });

	it('Renders an Routers', () => {
    expect(app.find(Router)).to.have.length(1);
  });

});
