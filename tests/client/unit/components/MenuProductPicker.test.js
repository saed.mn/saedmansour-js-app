import React from 'react';
import MenuProductPicker from '../../../../client/components/MenuProductPicker';
import { shallow, mount } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';

describe('<MenuProductPicker />', () => {
  let selectedProduct = "veggie-box";
  let handleChange = sinon.spy();

  it('renders a select', () => {
    const menuProductPicker = shallow(<MenuProductPicker selectedProduct={selectedProduct} onChange={handleChange} />);
    expect(menuProductPicker.find('select')).to.have.length(1);
  });

  it('on select change call function passed to handle change with correct arguments', () => {
    const menuProductPicker = mount(<MenuProductPicker selectedProduct={selectedProduct} onChange={handleChange} />);
    menuProductPicker.simulate('change', {target: { value : 'classic-box'}});
    expect(handleChange.calledOnce).to.be.true;
    expect(handleChange.getCall(0).args[0]).to.equal('classic-box');
  });

});
