import React from 'react';
import Menu from '../../../../client/components/Menu';
import Recipe from '../../../../client/components/Recipe';
import { shallow } from 'enzyme';
import { expect } from 'chai';
import sinon from 'sinon';

describe('<Menu />', () => {

  it('renders a div', () => {

    let recipes = [
        {recipe: { name: "Chicken", image: "saedmansour.com/image.jpeg", id: "1"}},
        {recipe: { name: "Chicken", image: "saedmansour.com/image.jpeg", id: "2"}}
      ];
    let selectedProduct = sinon.spy();

    const menu = shallow(<Menu recipes={recipes} selectedProduct={selectedProduct} />);
    expect(menu.find(Recipe)).to.have.length(2);
  });

});
