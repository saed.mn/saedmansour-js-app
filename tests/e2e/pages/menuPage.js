export default {
  url: 'http://localhost:3000/',
  elements: {
    menuListDescription: {
      selector: '//ul[@id="recipes"]',
      locateStrategy: 'xpath'
    }
  }
};
