const loginCommands = {
  login(email, pass) {
    return this
      .waitForElementVisible('@identifier')
      .setValue('@identifier', email)
      .setValue('@passInput', pass)
      .waitForElementVisible('@loginButton')
      .click('@loginButton')
  }
};

export default {
  url: 'http://localhost:3000/login',
  commands: [loginCommands],
  elements: {
    identifier: {
      selector: 'input[name=identifier]'
    },
    passInput: {
      selector: 'input[name=password]'
    },
    loginButton: {
      selector: 'button'
    }
  }
};
