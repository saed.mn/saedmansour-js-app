export default {
  'User Logs in': (client) => {
    const loginPage = client.page.loginPage();
    const menuPage = client.page.menuPage();

    loginPage
      .navigate()
      .login("saedmansour", "saedmansour");

    menuPage
      .navigate()
      .expect.element('@menuListDescription').to.be.present;

    client.end();
  }
};
