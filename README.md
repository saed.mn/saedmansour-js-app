JS learning project
=======================================

Things learned from building this:

- Using Webpack + Babel + React + Redux + Testing and building a full stack project from scratch.
- Authentication on JS side. (mostly done Ruby on Server)
- E2E Testing
- Jest
- Server code: migrations, db, middleware etc (too used to Ruby).
- Understanding Node more and how the ecosystem works across the stack.


Setup
------------

- `nvm use 6.6`
- `npm install -g nodemon`
- Install PostgreSQL.
- `createdb saedmansour-js-test`
- `cd server && knex migrate:latest && knex seed:run`
- `npm run start`
- `Login` (seeded username & password: saedmansour) ->  `Menu`
- Tests: `npm run test` or `jest` and `nightwatch` - Nightwatch may need configuration to run correctly - check nightwatch.json and replace the paths that include node_modules to suit your local packages. Also run `e2e-setup` after you do the configs.

Security notes
------------

- I've used my own authentication (inspired by a lot of internet stuff of course). I've built my own first with passport but saw the integration with React/Redux caused issues. Done some research online and saw that there's issues in combining both. So I saw the quicker path is to use something that works than risk digging deep too much for a homework project. For production I'd use ready package like Passport because it's more secure to depend on something that's maintained by a lot of people and always updated.

Acknowledgements
------------

Authentication logic (mainly and also parts of the project are inspired by) - I watched this entire 20 video playlist to help build this project:

- https://github.com/Remchi/reddice
- This entire tutorial youtube playlist: https://www.youtube.com/playlist?list=PLuNEz8XtB51K-x3bwCC9uNM_cxXaiCcRY
- svg images from codepen :) great place to find interesting stuff.

Conclusions, what to do better, TODO:
------------
- I did have my own git commit history previously but deleted it as it came on top of an github with Readme and I don't want to share another company's history. (just the ReadMe)
- Move LoginPage, SignUpPage, NavigationBar into components. Pass things in props. No need for so many containers.
