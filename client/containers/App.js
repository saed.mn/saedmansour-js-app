import React, { Component } from 'react';
import { connect } from 'react-redux'
import {BrowserRouter as Router, Link, Route, Redirect} from 'react-router-dom';
import Menu from '../components/menu';
import NavigationBar from './NavigationBar'
import LoginPage from './LoginPage'
import SignupPage from './SignupPage'
import {PRODUCT_OPTIONS} from '../components/MenuProductPicker'
import {getMenuFromAPI} from '../actions/menuActions'
import requireAuth from '../utils/requireAuth';


class App extends Component {

  propTypes: {
    recipes: PropTypes.array.isRequired,
    dispatch: PropTypes.func.isRequired,
    selectedProduct: PropTypes.string.isRequired
  }

  componentWillMount() {
    this.props.dispatch(getMenuFromAPI());
  }

  render() {
    const { recipes, selectedProduct, dispatch} = this.props

    const MenuWithProps = (props) => {
        return (<Menu recipes={recipes} selectedProduct={selectedProduct} dispatch={dispatch}  {...props} />);
    }

    return (

            <Router>
              <div id="app">
                <NavigationBar />
                <Route exact path='/' component={requireAuth(MenuWithProps)} />
                <Route exact path='/login' component={LoginPage} />
                <Route exact path='/signup' component={SignupPage} />
              </div>
            </Router>


    );
  }
}

const mapStateToProps = state => {
  let recipes;
  let selectedProduct;

  if (state && state.menu) {
    recipes = state.menu.recipes;
    selectedProduct = state.menu.selectedProduct;
  }
  else {
    recipes = [];
    selectedProduct = PRODUCT_OPTIONS[0].value;
  }

  return {
    recipes,
    selectedProduct
  }
}

export default connect(
  mapStateToProps
)(App)
