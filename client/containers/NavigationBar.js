import React, { Component } from 'react'
import { connect } from 'react-redux';
import { logout } from '../actions/authActions';
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types';
import {svgLogo} from '../assets/logo';

class NavigationBar extends Component {

  logout(e) {
    e.preventDefault();
    this.props.logout();
  }

  render() {

    const isAuthenticated = this.props.isAuthenticated;

    const userLinks = (
      <ul className="nav navbar-nav navbar-right">
        <li><a href="#" onClick={this.logout.bind(this)}>Logout</a></li>
      </ul>
    );

    const guestLinks = (
      <ul className="nav navbar-nav navbar-right">
        <li><Link to="/login">Login</Link></li>
      </ul>
    );

    // TODO: dangerouslySetInnerHTML, this is a quick thing for UI/UX. fix.
    return (
      <nav className="navbar navbar-default">
        <div className="container-fluid">
          <div className="navbar-header">
            <Link to="/" className="navbar-brand">
              <div className="logo" dangerouslySetInnerHTML={{__html: svgLogo}}></div>
            </Link>
          </div>

          <div className="">
            { isAuthenticated ? userLinks : guestLinks }
          </div>
        </div>
      </nav>
    );
  }
}

NavigationBar.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  logout: PropTypes.func.isRequired
}

function mapStateToProps(state) {
  return {
    isAuthenticated: state.auth.isAuthenticated
  };
}

export default connect(mapStateToProps, { logout })(NavigationBar);
