import React, { Component } from 'react';
import PropTypes from 'prop-types';
import LoginForm from '../components/LoginForm'; 
import { connect } from 'react-redux';
import { login } from '../actions/authActions';

class LoginPage extends Component {
  render() {
    const { login } = this.props;

    return (
      <div className="row">
        <div className="col-md-4 col-md-offset-4">
          <LoginForm login={login} />
          <form method="POST">
          </form>
        </div>
      </div>
    );
  }
}

LoginPage.propTypes = {
  login: PropTypes.func.isRequired,
}

export default connect(null, { login })(LoginPage);
