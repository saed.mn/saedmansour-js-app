import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SignupForm from '../components/SignupForm';
import { connect } from 'react-redux';
import { userSignupRequest, isUserExists } from '../actions/signupActions';

class SignupPage extends React.Component {
  render() {
    const { userSignupRequest, isUserExists } = this.props;
    return (
      <div className="row">
        <div className="col-md-4 col-md-offset-4">
          <SignupForm
            isUserExists={isUserExists}
            userSignupRequest={userSignupRequest} />
        </div>
      </div>
    );
  }
}

SignupPage.propTypes = {
  userSignupRequest: PropTypes.func.isRequired,
  isUserExists: PropTypes.func.isRequired
}

export default connect(null, { userSignupRequest, isUserExists })(SignupPage);
