import { GET_MENU, RECEIVE_MENU, SELECT_MENU_PRODUCT} from '../actions/types';
import {PRODUCT_OPTIONS} from '../components/MenuProductPicker';

const initialState = {
  recipes: [],
  selectedProduct: PRODUCT_OPTIONS[0].value,
}

export default function menu(state = initialState, action) {
  switch (action.type) {
    case GET_MENU:
      return {
        recipes: [],
        selectedProduct: state.selectedProduct
      };

    case RECEIVE_MENU:
      return {
        recipes: action.recipes,
        selectedProduct: state.selectedProduct
      };

    case SELECT_MENU_PRODUCT:
      return {
        recipes: state.recipes,
        selectedProduct: action.product
      };

    default:
      return state
  }
}
