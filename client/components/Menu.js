import React, { Component, PropTypes } from 'react';
import MenuProductPicker from './MenuProductPicker';
import Recipe from './Recipe';
import { selectMenuProduct, getMenuFromAPI } from '../actions/menuActions';

export default class Menu extends Component {

  propTypes: {
    recipes: PropTypes.array.isRequired,
    selectedProduct: PropTypes.string.isRequired,

  }

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(selectedProdct) {
    this.props.dispatch(selectMenuProduct(selectedProdct));
  }

  render() {
    const { selectedProduct, recipes, dispatch } = this.props;

    if(recipes.length > 0) {
      var recipesList = recipes.map(function(recipeWrapper) {
        const recipe = recipeWrapper.recipe;
        return (<Recipe name={recipe.name} imageURL={recipe.imageLink} key={recipe.id} />);
      });

      return(
        <div id="menu">
          <MenuProductPicker selectedProduct={selectedProduct} onChange={this.handleChange} />
          <ul id="recipes">{recipesList}</ul>
        </div>
      );
    }

    return(<div>Fetching recipes...</div>);
  }
};
