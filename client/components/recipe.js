import React, { Component, PropTypes } from 'react'

export default class Recipe extends Component {

  render() {
    const { name, imageURL } = this.props;

    return(
      <li className='recipe'>
        <div className='name'>{name}</div>
        <img src={imageURL} />
      </li>
    );
  }
}
