import React, { Component, PropTypes } from 'react'

export const PRODUCT_OPTIONS = [
  {value: 'classic-box', text: 'Classic Box'},
  {value: 'vegie-box', text: 'Vegie Box'}
];

export default class MenuProductPicker extends Component {

  render() {
    const { selectedProduct, onChange } = this.props;

    return(
      <select onChange={e => onChange(e.target.value)} value={selectedProduct}>
          {
            PRODUCT_OPTIONS.map(
              option =>
                <option value={option.value} key={option.value}>{option.text}</option>
            )
          }
      </select>
    );
  }
}
