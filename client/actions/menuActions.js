import * as types from './types'

const selectProduct = (product) => ({
    type: types.SELECT_MENU_PRODUCT,
    product
});

const getMenu = () => ({
    type: types.GET_MENU
});

const receiveMenu = (json) => {
  return ({
    type: types.RECEIVE_MENU,
    recipes: json.items[0].courses
  });
};

const getAccessToken = () => {
  return fetch('https://api-v2.hellofresh.com/auth/oauth2/client/access_token', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        client_id: "hellofresh-dev-test",
        client_secret: "g4c25EzG4#%Afeh07Bb#anbH5BQQ67bJ7!G6QZOA",
        grant_type: "client_credentials",
        scope: "public"
      })
    });
}

export const getMenuFromAPI = (product = 'classic-box') => (dispatch) => {
  dispatch(getMenu());

  getAccessToken()
    .then(response => response.json())
    .then(response => {
      var result =  fetch("https://api-v2.hellofresh.com/menus?week=current&country=US&product=" + product,
        {
          headers: {
            'Content-Type': 'application/json',
            'Authorization': "Bearer " + response.access_token
          }
        }
      )
    .then(response => response.json())
    .then(json => dispatch(receiveMenu(json)));
    });
};

export const selectMenuProduct = (product) => (dispatch) => {
  dispatch(selectProduct(product));
  dispatch(getMenuFromAPI(product));
};
