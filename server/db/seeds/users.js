exports.seed = function(knex, Promise) {
  return knex('users').del()
    .then(function () {
      return knex('users').insert([
        {id: 1, username: 'saedmansour', email: 'saed.mn@gmail.com', password_digest: '$2a$10$Sc9.VSagUDUW7nZ5rrBgk.Kq9Ki7d3nDBIL6KFbfftjjXPXQIoEA6'}
      ]);
    });
};
