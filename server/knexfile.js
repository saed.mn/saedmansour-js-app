module.exports = {

  development: {
    client: 'postgresql',
    connection: {
      database: 'saedmansour-js-test',
      user:     'saed',
      password: ''
    },
    seeds: {
      directory: './db/seeds/'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  staging: {
    client: 'postgresql',
    connection: {
      database: 'saedmansour-js-test',
      user:     'saed',
      password: ''
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'postgresql',
    connection: {
      database: 'saedmansour-js-test',
      user:     'saed',
      password: ''
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }

};
