import express from 'express';
import path from 'path'
import webpack from 'webpack';
import webpackMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpackConfig from '../webpack.config.dev.js';
import bodyParser from 'body-parser';
import users from './routes/users';
import auth from './routes/auth';

const app = express();
const compiler = webpack(webpackConfig);

app.use(webpackMiddleware(compiler, {
  hot: true,
  publicPath: webpackConfig.output.publicPath,
  noInfo: true
}));

app.use(webpackHotMiddleware(compiler));

app.use(bodyParser.json());

app.use(express.static(path.join(__dirname,'../client/assets') ));

app.use('/api/auth', auth);
app.use('/api/users', users);
app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, './index.html'));
});

app.listen(3000, () => console.log('Running on localhost:3000'));
